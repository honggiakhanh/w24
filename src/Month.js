import React, {useState} from 'react'
import Day from './Day'

const Month = () => {
    const months = [1,2,3,4,5,6,7,8,9,10,11,12]
    const [month, setMonth] = useState(1);
    
    return (
        <div>
            <select value={month} onChange={(e) => setMonth(e.target.value)}>
                {months.map(m => <option>{m}</option>)}
            </select>
            <Day month={month}/>
        </div>
    )
}

export default Month
