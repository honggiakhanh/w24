import React from 'react'

const Day = (props) => {
    const day = new Date(`${props.month}/1, 2021`)
    const weekday = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];

    return (
        <div>
            Month {props.month}/2021 starts {weekday[day.getDay()]}
        </div>
    )
}

export default Day
